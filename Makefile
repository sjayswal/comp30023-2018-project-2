CC		= gcc

RM		= rm -f


default: all

all: certcheck

certcheck: certexample.c
	$(CC) certexample.c -l ssl -l crypto -o certcheck 

clean veryclean:
	$(RM) certcheck


